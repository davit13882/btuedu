package com.randomgenerator.randomnumber

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val randomGeneratorButton = findViewById<Button>(R.id.randomGeneratorButton)
        val randomNumberText = findViewById<TextView>(R.id.randomNumberText)
        randomGeneratorButton.setOnClickListener {
            val number = randomNumber()
            randomNumberText.text = number.toString()
            divise(number)


        }
    }
    private fun randomNumber() = (-100..100).random()

    fun divise(number:Int){
        val divisibleText = findViewById<TextView>(R.id.Divisible)
        if (number % 5 == 0) {
            divisibleText.text = "Yes"
        } else {
            divisibleText.text = "No"
        }
    }
}